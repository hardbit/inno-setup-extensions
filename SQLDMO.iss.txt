[CustomMessages]
SQLServer_Caption=SQL Server
SQLServer_Description=Ingrese la informaci�n solicitada de SQL Server
SQLServer_NameServerTxt_Caption=Nombre del servidor:
SQLServer_RefreshListSqlServersCmd_Caption=&Actualizar
SQLServer_LoginSecureChb_Caption=Autentificacion de windows
SQLServer_UserNameLbl_Caption=Usuario:
SQLServer_PasswordLbl_Caption=Contrase�a:
SQLServer_TestConnectionCmd_Caption=&Probar conexion
SQLServer_SelectOperationLbl_Caption=Selecciona la operacion a realizar:
SQLServer_AttachDbFileRb_Caption=Adjuntar base de datos:
SQLServer_ExecuteScriptSqlRb_Caption=Ejecutar archivo de script:
SQLServer_PathFileLbl_Caption=Ruta del archivo:
SQLServer_PathFileCmd_Caption=Abrir..
SQLServer_ExecOperationCmd_Caption=Ejecutar peracion
SQLServer_ConnectedSucessful=Se conecto satisfactoriamente a
SQLServer_ConnectedFailed=Error al intentar conectar con
BlankServerName=El nombre del servidor es obligatorio

[Code]
var
  Bevel1: TBevel;
  NameServerTxt: TNewStaticText;
  ListServerSqlCmb: TComboBox;
  RefreshListSqlServersCmd: TButton;
  LoginSecureChb: TCheckBox;
  UserNameLbl: TNewStaticText;
  PasswordLbl: TNewStaticText;
  UserNameTxt: TEdit;
  PasswordTxt: TPasswordEdit;
  TestConnectionCmd: TButton;

  ConnectionSuccessful: Boolean;
  SqlServerPage: Integer;

procedure GetListAvailableSQLServers();
var
  SqlApp, NameList: Variant;
  x: Integer;
begin
  SqlApp := CreateOleObject('SQLDMO.Application');
  NameList := SqlApp.ListAvailableSQLServers();

  for x:= 1 to NameList.Count do
  begin
  ListServerSqlCmb.Items.Add(NameList.Item[x]);
  end;
end;

{ SQLServer_Activate }

procedure SQLServer_Activate(Page: TWizardPage);
begin
  ConnectionSuccessful := False;
end;

{ SQLServer_ShouldSkipPage }

function SQLServer_ShouldSkipPage(Page: TWizardPage): Boolean;
begin
  Result := False;
end;

{ SQLServer_BackButtonClick }

function SQLServer_BackButtonClick(Page: TWizardPage): Boolean;
begin
  Result := True;
end;


{ SQLServer_NextkButtonClick }

function SQLServer_NextButtonClick(Page: TWizardPage): Boolean;
begin
  Result := True

  if ConnectionSuccessful = False Then
  begin
  if MsgBox('No se ha creado una conexion, �Desea continuar con la instalacion?' + #13 + 'Si continua no se instalara la base de datos.', mbConfirmation, MB_YESNO) = IDNO then
  begin
  Result := False
  end;
  end;
end;

{ SQLServer_CancelButtonClick }

procedure SQLServer_CancelButtonClick(Page: TWizardPage; var Cancel, Confirm: Boolean);
begin
  // enter code here...
end;

procedure RefreshListAvailableSQLServers(Sender: TObject);
begin
  GetListAvailableSQLServers();
end;

procedure TestConnection(Sender: TObject);
var
  SQLServer: Variant;
begin
  try
    SQLServer := CreateOleObject('SQLDMO.SQLServer');
  except
    RaiseException('Please install Microsoft SQL server connectivity tools first.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;

  if ListServerSqlCmb.Text = '' Then
  begin
  MsgBox(ExpandConstant('{cm:BlankServerName}'), mbError, MB_OK);
  end
  else
  begin

  try
  if LoginSecureChb.Checked = True Then
  begin
  SQLServer.LoginSecure := True;
  SQLServer.Connect(ListServerSqlCmb.Text, '', '');
  end
  else
  begin
  SQLServer.Connect(ListServerSqlCmb.Text, UserNameTxt.Text, PasswordTxt.Text);
  end;

  MsgBox(ExpandConstant('{cm:SQLServer_ConnectedSucessful} ') + ListServerSqlCmb.Text + '.', mbInformation, mb_Ok);
  ConnectionSuccessful := True;
  except
    MsgBox(ExpandConstant('{cm:SQLServer_ConnectedFailed} ') + ListServerSqlCmb.Text + '.', mbError, MB_OK);
    ConnectionSuccessful := False;
  end;

  end;
end;

procedure LoginSecureChecked(Sender: TObject);
begin
  if (LoginSecureChb.Checked = True) Then
  begin
  Bevel1.Enabled := False;
  UserNameTxt.Enabled := False;
  UserNameLbl.Enabled := False;
  PasswordTxt.Enabled := False;
  PasswordLbl.Enabled := False;
  end
  else
  begin
  Bevel1.Enabled := True;
  UserNameTxt.Enabled := True;
  UserNameLbl.Enabled := True;
  PasswordTxt.Enabled := True;
  PasswordLbl.Enabled := True;
  end;
end;

{ SQLServer_CreatePage }

function SQLServer_CreatePage(PreviousPageId: Integer): Integer;
var
  Page: TWizardPage;
begin
  Page := CreateCustomPage(
    PreviousPageId,
    ExpandConstant('{cm:SQLServer_Caption}'),
    ExpandConstant('{cm:SQLServer_Description}')
  );

{ Bevel1 }
  Bevel1 := TBevel.Create(Page);
  with Bevel1 do
  begin
    Parent := Page.Surface;
    Left := ScaleX(16);
    Top := ScaleY(79);
    Width := ScaleX(377);
    Height := ScaleY(69);
    Shape := bsFrame;
  end;

  { NameServerTxt }
  NameServerTxt := TNewStaticText.Create(Page);
  with NameServerTxt do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_NameServerTxt_Caption}');
    Left := ScaleX(16);
    Top := ScaleY(15);
    Width := ScaleX(101);
    Height := ScaleY(14);
    TabOrder := 0;
  end;

  { ListServerSqlCmb }
  ListServerSqlCmb := TComboBox.Create(Page);
  with ListServerSqlCmb do
  begin
    Parent := Page.Surface;
    Left := ScaleX(16);
    Top := ScaleY(32);
    Width := ScaleX(289);
    Height := ScaleY(21);
    TabOrder := 1;
  end;

  { RefreshListSqlServersCmd }
  RefreshListSqlServersCmd := TButton.Create(Page);
  with RefreshListSqlServersCmd do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_RefreshListSqlServersCmd_Caption}');
    Left := ScaleX(320);
    Top := ScaleY(31);
    Width := ScaleX(75);
    Height := ScaleY(23);
    TabOrder := 2;
  end;

  { LoginSecureChb }
  LoginSecureChb := TCheckBox.Create(Page);
  with LoginSecureChb do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_LoginSecureChb_Caption}');
    Left := ScaleX(24);
    Top := ScaleY(72);
    Width := ScaleX(153);
    Height := ScaleY(17);
    TabOrder := 3;
  end;

  { UserNameLbl }
  UserNameLbl := TNewStaticText.Create(Page);
  with UserNameLbl do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_UserNameLbl_Caption}');
    Left := ScaleX(48);
    Top := ScaleY(96);
    Width := ScaleX(41);
    Height := ScaleY(14);
    TabOrder := 4;
  end;

  { PasswordLbl }
  PasswordLbl := TNewStaticText.Create(Page);
  with PasswordLbl do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_PasswordLbl_Caption}');
    Left := ScaleX(48);
    Top := ScaleY(120);
    Width := ScaleX(61);
    Height := ScaleY(14);
    TabOrder := 5;
  end;

  { UserNameTxt }
  UserNameTxt := TEdit.Create(Page);
  with UserNameTxt do
  begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(93);
    Width := ScaleX(241);
    Height := ScaleY(21);
    TabOrder := 6;
  end;

  { PasswordTxt }
  PasswordTxt := TPasswordEdit.Create(Page);
  with PasswordTxt do
  begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(118);
    Width := ScaleX(241);
    Height := ScaleY(21);
    TabOrder := 7;
  end;

  { TestConnectionCmd }
  TestConnectionCmd := TButton.Create(Page);
  with TestConnectionCmd do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:SQLServer_TestConnectionCmd_Caption}');
    Left := ScaleX(16);
    Top := ScaleY(158);
    Width := ScaleX(91);
    Height := ScaleY(23);
    TabOrder := 8;
  end;

  with Page do
  begin
    OnActivate := @SQLServer_Activate;
    OnShouldSkipPage := @SQLServer_ShouldSkipPage;
    OnBackButtonClick := @SQLServer_BackButtonClick;
    OnNextButtonClick := @SQLServer_NextButtonClick;
    OnCancelButtonClick := @SQLServer_CancelButtonClick;
  end;

  Result := Page.ID;

  RefreshListSqlServersCmd.OnClick := @RefreshListAvailableSQLServers
  TestConnectionCmd.OnClick := @TestConnection;
  LoginSecureChb.OnClick := @LoginSecureChecked;
end;

{ Example }
{
procedure InitializeWizard();
begin
  SqlServerPage := SQLServer_CreatePage(wpWelcome);
end;

procedure SQLExec();
begin
  MsgBox('Aqui realize la operacion para SQL Server =)', mbinformation, mb_ok);
end;

function NextButtonClick(CurPageID: Integer): Boolean;
begin
  if CurPageID = SqlServerPage Then
  begin
  if ConnectionSuccessful = True then
  begin
	SQLExec();
  end;
  end;

  Result := True;
end;
}
